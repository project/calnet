
Developer Notes
----------------
You need to manually include a link to ?q=calnet/register to start the registration
process.

To Do
----------------
- Change copyright notice, but make sure to include attribution in the form of
"with support from CommerceNet, Inc." or something like that.
- There is probably a bit of Big Ideas specific code in there so that needs to be
cleaned up.
- Security audit before release.
- Upgrade to Drupal 5.x.
- I didn't include aws.php since you need a CalNet login to get this code in the
first place. Should put some error handling and message in there instead of
require_once('aws.php').

Author
----------------
Initially developed by Rob Barreca <rob at chipin dot com> of ChipIn. Module
development sponsored by CommerceNet and ChipIn for the Big Ideas @ Berkeley
Initiative.
